module.exports = {
  root: true,
  extends: [
    'standard-with-typescript',
    'standard-jsx',
    'standard-react',
    'plugin:jsx-a11y/recommended',
    'plugin:jest/recommended',
    'prettier',
  ],
  plugins: [
    'import',
    'prettier',
    'react',
    'react-hooks',
    '@typescript-eslint',
    'jsx-a11y',
    'spellcheck',

    // not used in this config but necessary for overrides
    'unicorn',
  ],
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  env: {
    commonjs: true,
    browser: true,
    es6: true,
    jest: true,
  },
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    ecmaFeatures: {
      jsx: true, // Allows for the parsing of JSX
      arrowFunctions: true,
    },
  },
  settings: {
    react: {
      version: 'detect', // Tells eslint-plugin-react to automatically detect the version of React to use
    },
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        paths: ['./src'],
      },
    },
  },
  rules: {
    // General
    'one-var': ['error', 'never'],

    // Prettier
    'prettier/prettier': 'error',

    // React
    'react-hooks/exhaustive-deps': 'error',
    'react-hooks/rules-of-hooks': 'error',
    'react/prop-types': 'off',
    'react/require-default-props': 'off',
    'react/jsx-handler-names': 'off',

    // Typescript
    '@typescript-eslint/array-type': 'off',
    '@typescript-eslint/consistent-type-assertions': 'off',
    '@typescript-eslint/consistent-type-definitions': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/method-signature-style': 'off',
    '@typescript-eslint/restrict-plus-operands': 'off',
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'default',
        format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
        leadingUnderscore: 'allow',
        trailingUnderscore: 'allow',
      },
      {
        selector: ['objectLiteralProperty', 'typeProperty'],
        format: null,
      },
      {
        selector: 'typeLike',
        format: ['PascalCase'],
        leadingUnderscore: 'allow',
      },
    ],
    '@typescript-eslint/no-explicit-any': ['error', { ignoreRestArgs: true }],
    '@typescript-eslint/require-array-sort-compare': ['warn', { ignoreStringArrays: true }],
    '@typescript-eslint/no-unused-expressions': [
      'error',
      {
        allowShortCircuit: true,
        allowTaggedTemplates: true,
        allowTernary: true,
      },
    ],
    '@typescript-eslint/no-unused-vars': [
      'error',
      { args: 'none', ignoreRestSiblings: true, varsIgnorePattern: '^_' },
    ],
    '@typescript-eslint/prefer-nullish-coalescing': [
      'warn',
      {
        forceSuggestionFixer: true,
        ignoreConditionalTests: false,
        ignoreMixedLogicalExpressions: false,
      },
    ],
    '@typescript-eslint/restrict-template-expressions': 'off',
    '@typescript-eslint/strict-boolean-expressions': 'off',

    // Fixes
    'no-void': 'off', // fix for conflict with @typescript-eslint/no-floating-promises

    // Spelling
    'spellcheck/spell-checker': [
      'warn',
      {
        comments: false,
        strings: true,
        identifiers: false,
        lang: 'en_US',
        skipWords: [
          'dict',
          'aff',
          'hunspellchecker',
          'hunspell',
          'utils',
          'aws',
          'loree',
          'iframe',
        ],
        skipIfMatch: [
          'http://[^s]*',
          '^[-\\w]+/[-\\w\\.]+$', // For MIME Types
        ],
        skipWordIfMatch: [
          // '^foobar.*$', // words that begin with foobar will not be checked
        ],
        minLength: 3,
      },
    ],

    'unicorn/no-abusive-eslint-disable': 'off',
  },
  overrides: [
    {
      files: ['**/*.+(ts|tsx)'],
      rules: {
        '@typescript-eslint/no-unsafe-member-access': 'warn',
      },
    },
    {
      // Lower severity in tests for particularly annoying rules
      files: ['**/__tests__/*.+(js|jsx|ts|tsx)', '**/*.+(test|spec).+(js|jsx|ts|tsx)'],
      rules: {
        '@typescript-eslint/no-explicit-any': 'warn',
        '@typescript-eslint/no-floating-promises': 'warn',
        '@typescript-eslint/no-non-null-assertion': 'warn',
      },
    },
  ],
  ignorePatterns: [
    '/amplify/#current-cloud-backend',
    '/amplify/backend',
    '/build',
    '/coverage',
    '/dist',
    '/lms-assets/themes.js',
    '/node_modules',
    '/public/ckeditor/*',
    '/public/jquery/*',
    '/public/select2/*',
    '/public/scripts/axe.min.js',
    '/src/API.ts',
    '/src/graphql/*',
    '/src/react-app-env.d.ts',
    '/src/serviceWorker.ts',
    '/src/setupTests.ts',
  ],
};
